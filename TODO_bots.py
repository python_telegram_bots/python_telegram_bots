from random import choice
import telebot
from telebot import types

token = ''

bot = telebot.TeleBot(token)


RANDOM_TASKS = ['Сделать разминку', 'Посмотреть лекцию по Python', 'Решить 3 задачи на Python', 'Добавить фичу в TODO бота', \
'Выпить стокан воды']

HELP = '''
Список доступных команд:
/show  - напечать все задачи на заданную дату
/add - добавить задачу
/random - добавить на сегодня случайную задачу
/help - Напечатать help
В разработке:
* * *
'''

todos = dict()

# Buttons
but_show = "Напечать все задачи на заданную дату"
but_add = "Добавить задачу"
but_random = "Добавить на сегодня случайную задачу"
but_help = "HELP"

# Функция, обрабатывающая команду /start
@bot.message_handler(commands=["start"])
def start(message, res=False):
    bot.send_message(message.chat.id, 'Привет! Готов к работе (=')

    # Добавляем две кнопки
    markup=types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1=types.KeyboardButton(but_show)
    item2=types.KeyboardButton(but_add)
    item3=types.KeyboardButton(but_random)
    item4=types.KeyboardButton(but_help)
    markup.add(item1)
    markup.add(item2)
    markup.add(item3)
    markup.add(item4)
    bot.send_message(message.chat.id, 'Жмякай на кнопки', reply_markup=markup)


def add_todo(date, task):
    date = date.lower()
    if todos.get(date) is not None:
        todos[date].append(task)
    else:
        todos[date] = [task]


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(message.chat.id, HELP)


@bot.message_handler(commands=['random'])
def random(message):
    task = choice(RANDOM_TASKS)
    add_todo('сегодня', task)
    bot.send_message(message.chat.id, f'Задача: * {task} * добавлена на сегодня')


@bot.message_handler(commands=['add'])
def add(message):
    _, date, tail = message.text.split(' ', 2)
    task = ' '.join([tail])
    add_todo(date, task)
    bot.send_message(message.chat.id, f'Задача: * {task} * добавлена на дату {date}')


@bot.message_handler(commands=['show'])
def show(message):
    date = message.text.split()[1].lower()
    if date in todos:
        tasks = ''
        for task in todos[date]:
            tasks += f' - {task}\n'
    else:
        tasks = 'Такой даты нет'
    bot.send_message(message.chat.id, tasks)


# Получение сообщений от юзера
@bot.message_handler(content_types=["text"])
def handle_text(message):
    # Если юзер прислал 1
    if message.text.strip() == but_show: pass
    # Если юзер прислал 2
    elif message.text.strip() == but_add: pass
    # Если юзер прислал 3
    elif message.text.strip() == but_random: pass
    # Если юзер прислал 4
    elif message.text.strip() == but_help: pass


bot.polling(none_stop=True, interval=0)
